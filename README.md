# Pizza Bot
A bot for interacting with Piazza

# Setup

Copy `config.example.json` to `config.json`, and replace the placeholder values with your own, adding and removing courses and their network IDs as needed.

To get the network ID, log in to piazza and go to a course. The network ID is everything after the `/class/` and before the first `?` (if present). This is needed for every course.

## Linux

This bot uses python3, relying on discord.py and piazza-api. To get set up, simply run the commands below (assumes sh, adjust for other shells accordingly).

```sh
# Set up venv
python -m venv venv

# Configure shell to use venv
# for other shells, source file with correct extension
source venv/bin/activate.sh

# Install dependencies
pip install discord.py
pip install piazza-api
```
