from typing import Dict, List
from piazza_api import Piazza
from piazza_api.network import Network
from piazza_api.exceptions import RequestError
import discord
import json
import html
import re


class DiscordConfig():
    def __init__(self, config: dict):
        self.token: str = config['token']
        self.prefix: str = config['prefix']


class PiazzaConfig():
    def __init__(self, config: dict):
        self.email: str = config['email']
        self.password: str = config['password']
        self.courses: Dict[str, str] = dict()
        for name, id in config['courses'].items():
            self.courses[name.lower()] = id


class Config():
    def __init__(self, config_path: str = 'config.json'):
        with open(config_path) as config_file:
            config = json.load(config_file)
            self.discord = DiscordConfig(config['discord'])
            self.piazza = PiazzaConfig(config['piazza'])


class PizzaBot(discord.Client):

    @staticmethod
    def cleanup_content(content: str):
        # removes html tags from content (I hope)
        content = html.unescape(content)
        content = content.replace("<p>", "\n")
        content = content.replace("</p>", "")
        content = content.replace("<code>", "\n```")
        content = content.replace("</code>", "```")
        content = content.replace("<br />", "")
        content = content.replace('<a href="/', "https://piazza.com/")
        content = content.replace('<img src="/', "https://piazza.com/")
        content = content.replace('<a href="', "")
        content = content.replace('<img src="', "")
        content = content.replace('" />', " ")
        content = content.replace('">', " ")
        content = content.replace('" alt="" />', '')
        content = content.replace('</a>', '')
        content = content.replace('<strong>', '**')
        content = content.replace('</strong>', '**')
        content = content.replace('<em>', '*')
        content = content.replace('</em>', '*')
        content = content.replace("<md>", "").strip().replace("</md>", "")

        # condenses multiple newlines to single newline
        content = re.sub(r'[\r\n][\r\n]{2,}', '\n\n', content).strip()
        return content

    @staticmethod
    def get_feed(course: Network, count: int = 5):
        feed = course.get_feed(count)["feed"]

        return PizzaBot.parse_feed(course, feed)

    @staticmethod
    def parse_feed(course: Network, feed):
        summaries = ""
        cnt = 0
        for post in feed:
            title = PizzaBot.cleanup_content(post["subject"])
            title = title.replace("*", "\\*")
            summary = PizzaBot.cleanup_content(post["content_snipet"])
            summary = summary.replace("\n", "\n> ") + "..."
            url = f'https://piazza.com/class/{course._nid}?cid={post["nr"]}'
            summary = f'\n**{title}**\n> {summary}\n{url}\n'
            if len(summaries) + len(summary) > 1900:
                break
            cnt = cnt + 1
            summaries = summaries + summary
        url = f'https://piazza.com/{course._nid}'
        summaries = f'First {cnt} results from {url}:\n{summaries}'
        return summaries

    @staticmethod
    def get_post(course: Network, id: int):
        post = course.get_post(id)
        # TODO: handle poll
        type = post["type"]
        print(type)
        tags = post["tags"]  # todo: display tags

        title = post["history"][0]["subject"]
        title = PizzaBot.cleanup_content(title)

        content = post["history"][0]["content"]
        content = PizzaBot.cleanup_content(content)
        content = content.strip().replace("\n", "\n> ")

        if type == "question":
            if len(post["children"]) == 0:
                answer = "There has been no answer yet."
            else:
                answer = post["children"][0]
                for new_answer in post["children"][1:]:
                    if new_answer["is_tag_endorse"] or new_answer["i_answer"]:
                        answer = new_answer
                answer = answer["history"][0]["content"]
                answer = PizzaBot.cleanup_content(answer).replace("\n", "\n> ")
            return {"title": title, "question": content, "answer": answer}
        elif type == "note":
            return {"title": title, "note": content}
        elif type == "poll":
            results = post["data"]["result"]
            return {"title": title, "poll": content, "result": results}
        else:
            print(f'Post of type {type} is unimplemented!')
            print(post)
            return f'Post of type {type} is unimplemented!'

    @staticmethod
    def search_posts(course: Network, query: str):
        feed = course.search_feed(query)
        return PizzaBot.parse_feed(course, feed)

    def setup(self, config: Config, piazza: Piazza):
        self.courses: Dict[str, Network] = {}
        self.prefix = config.discord.prefix
        for name, id in config.piazza.courses.items():
            self.courses[name] = piazza.network(id)

    async def on_ready(self):
        print(f'{self.user} is now serving hot and ready answers!')

    async def on_message(self, message):
        if message.author.bot:
            return
        print('message recieved\n> {0.author}: {0.content}'.format(message))

        if not message.content.strip().startswith(self.prefix):
            return

        # Must be at least as long as prefix
        command: str = message.content[len(self.prefix):].strip().lower()

        if len(command) == 0:
            await message.channel.send("No class or id specified.")
            return

        query: List[str] = command.lower().split()[:2]

        if len(query) == 1:
            # Try second delimmiter
            query = query[0].split("@")[:2]

        if query[0] in self.courses:
            try:
                course: Network = self.courses[query[0]]
                url = f'https://piazza.com/class/{course._nid}'
                if len(query) == 1:
                    # get the class feed
                    feed = self.get_feed(course, 10)
                    feed = f'{url}\n{feed}'
                    print(feed)
                    await message.channel.send(feed)
                else:
                    if query[1].isdigit():
                        # get post by id
                        post_id = int(query[1])
                        url = f"{url}?cid={post_id}"

                        post = self.get_post(course, post_id)
                        title = "**" + post["title"].replace("*", "\\*") + "**"

                        if "question" in post:
                            summary = f'{title}\n> {post["question"]}\n\n**Answer**\n> {post["answer"]}'
                        elif "note" in post:
                            summary = f'{title}\n> {post["note"]}'
                        elif "poll" in post:
                            summary = f'{title}\n> {post["poll"]}\n\n**Results**'
                            for name, count in post["result"].items():
                                summary = summary + f"\n> {name}: {count}"
                        else:
                            print("Unimplemented!")
                        summary = f'{url}\n{summary}'
                        await message.channel.send(summary)
                    else:
                        # search by string
                        query[1] = command.lower()[len(query[0]):].strip()
                        print(query[1])
                        if len(query[1]) < 3:
                            await message.channel.send("Seach must be at least 3 characters long!")
                            return
                        results = self.search_posts(course, query[1])
                        await message.channel.send(results)
            except RequestError:
                await message.channel.send("Invalid Post ID")
        else:
            await message.channel.send("Invalid Course name")


config = Config()

bot = PizzaBot()

p = Piazza()

p.user_login(config.piazza.email, config.piazza.password)

bot.setup(config, p)
bot.run(config.discord.token)
